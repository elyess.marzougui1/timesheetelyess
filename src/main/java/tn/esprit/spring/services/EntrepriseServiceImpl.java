package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.iterators.ArrayListIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.controller.RestControlEmploye;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

@Service
public class EntrepriseServiceImpl implements IEntrepriseService {

	@Autowired
    EntrepriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;
	
	Logger logger = LoggerFactory.getLogger(RestControlEmploye.class); 
	public int ajouterEntreprise(Entreprise entreprise) {
		entrepriseRepoistory.save(entreprise);
		return entreprise.getId();
	}

	public int ajouterDepartement(Departement dep) {
		logger.info("Adding department");
		try {
		deptRepoistory.save(dep);
		logger.info("Department Added Succefully !");
		}catch(Exception e) {
			logger.error(e.toString());
		}
		return dep.getId();
	}
	
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		if (Integer.toString(entrepriseId) == "") {
			logger.error("No entreprise to Add !");
		}
		else if (Integer.toString(depId) == "") {
			logger.error("No Departement To link entreprise to !");
		}else {
			logger.info("Adding Departement to entreprise");

				Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
				Departement depManagedEntity = deptRepoistory.findById(depId).get();
				
				depManagedEntity.setEntreprise(entrepriseManagedEntity);
				deptRepoistory.save(depManagedEntity);}
		
	}
	
	
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		List<String> depNames = new ArrayList<String>(); 
		
		if (Integer.toString(entrepriseId) == null) {
	    	   logger.warn("No entreprise ! ");
	       }else {
		Entreprise entrepriseManagedEntity = entrepriseRepoistory.findById(entrepriseId).get();
		try {
		for(Departement dep : entrepriseManagedEntity.getDepartements()){
			depNames.add(dep.getName());
		}
	       }
	       catch (Exception e) {
			logger.error(e.toString());
		}
	       }
		
		return depNames;
		
	}

	@Transactional
	public void deleteEntrepriseById(int entrepriseId) {
		entrepriseRepoistory.delete(entrepriseRepoistory.findById(entrepriseId).get());	
	}

	@Transactional
	public void deleteDepartementById(int depId) {
		if (Integer.toString(depId) == null) {
	    	   logger.warn("No departement ! ");
	       }else {
	    	   try {
		deptRepoistory.delete(deptRepoistory.findById(depId).get());	
	}catch(Exception e) {
		logger.error(e.toString());

	}
		
	}
	}


	public Entreprise getEntrepriseById(int entrepriseId) {
		return entrepriseRepoistory.findById(entrepriseId).get();	
	}

}
