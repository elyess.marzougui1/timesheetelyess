package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.controller.RestControlEmploye;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.repository.DepartementRepository;

@Service
public class DepartementServiceImpl implements IDepartementService {


	@Autowired
	DepartementRepository deptRepoistory;
	
	Logger logger = LoggerFactory.getLogger(DepartementServiceImpl.class);

	public List<Departement> getAllDepartements() {
		List<Departement> deps = new ArrayList<>() ; 
		
		logger.info("Excuting getAllDepartements");
		
		try {
		deps = 	 (List<Departement>) deptRepoistory.findAll();
		logger.info("Action Completed ! ");
		 }catch(Exception e) {
			 logger.error(e.toString());
		 }
		if (deps.size()  == 0) {
			logger.warn("no departments found !"); 
		}
		return deps ; 
		
	}

}
