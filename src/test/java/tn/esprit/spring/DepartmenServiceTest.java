package tn.esprit.spring;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.services.IDepartementService;
import tn.esprit.spring.services.IEntrepriseService;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class DepartmenServiceTest {
	
	@Autowired
	private IDepartementService departementService ; 
	
	@Autowired
	private IEntrepriseService entrepriseService ; 
	
	Departement dep = new Departement("Dep"); 
	
	
	@Test
	@Order(1)
	public void testAjoutdep () {
		entrepriseService.ajouterDepartement(dep); 
	}
	
	@Test
	@Order(2)
	public void testGetAll () {
		departementService.getAllDepartements(); 
	}
	
	@Test
	@Order(3)
	public void testAffecteraentrep () {
		int depId = 0 ; 
		List <Departement> deps = departementService.getAllDepartements();
		for (Departement deep : deps) {
			depId = deep.getId(); 
		}

		entrepriseService.affecterDepartementAEntreprise(depId, 1);
	}
	
	@Test
	@Order(4)
	public void testDepNameByEntreprise () {
		entrepriseService.getAllDepartementsNamesByEntreprise(1); 
	}
	
	@Test
	@Order(5)
	public void testdeleteDep () {
		
		int depId = 0 ; 
		List <Departement> deps = departementService.getAllDepartements();
		for (Departement deep : deps) {
			depId = deep.getId(); 
		}

		entrepriseService.deleteDepartementById(depId);
		
	}
	

}
